package com.java80.littlegame.service.gamelogic.caiquan;

import java.util.ArrayList;
import java.util.List;

import com.java80.littlegame.service.game.desk.BasePlayer;

public class CaiquanPlayer extends BasePlayer {
	private List<Byte> actions = new ArrayList<>();
	private byte chuquan;// 出的什么券

	public byte getChuquan() {
		return chuquan;
	}

	public void setChuquan(byte chuquan) {
		this.chuquan = chuquan;
	}

	public List<Byte> getActions() {
		return actions;
	}

	public void setActions(List<Byte> actions) {
		this.actions = actions;
	}
}
