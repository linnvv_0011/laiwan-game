package com.java80.littlegame.service.room;

import com.hazelcast.config.Config;
import com.java80.littlegame.common.base.BaseHandler;
import com.java80.littlegame.common.base.BaseVerticle;
import com.java80.littlegame.common.base.Runner;
import com.java80.littlegame.common.base.ServiceStatus;
import com.java80.littlegame.common.base.SystemConsts;

import io.vertx.core.VertxOptions;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class RoomVerticle extends BaseVerticle {

	@Override
	public String queueName() {
		return RoomConfig.getQueueName();
	}

	@Override
	public BaseHandler getHandler() {
		return new RoomHandler();
	}

	@Override
	public ServiceStatus getServiceStatus() {
		ServiceStatus ss = new ServiceStatus();
		ss.setInstanceName(RoomConfig.getInstanceName());
		ss.setServiceId(RoomConfig.getServiceId());
		ss.setServiceQueueName(queueName());
		ss.setServiceType(SystemConsts.SERVICE_TYPE_ROOM);
		return ss;
	}

	@Override
	public boolean needPublishServiceStatus() {
		return true;
	}

	public static void main(String[] args) {
		Runner.run(RoomVerticle.class,
				new VertxOptions()
						.setClusterManager(new HazelcastClusterManager(new Config(RoomConfig.getInstanceName())))
						.setClustered(true));
	}
}
