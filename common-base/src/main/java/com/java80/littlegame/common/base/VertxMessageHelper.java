package com.java80.littlegame.common.base;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java80.littlegame.common.message.proto.BaseMsg;

import io.vertx.core.AbstractVerticle;

public class VertxMessageHelper extends AbstractVerticle {
	private static VertxMessageHelper instance = null;
	final transient static Logger log = LoggerFactory.getLogger(VertxMessageHelper.class);
	private static ConcurrentLinkedQueue<BaseMsg> msgs = new ConcurrentLinkedQueue<>();

	@Override
	public void start() throws Exception {
		super.start();
		instance = this;
	}

	public static void sendMessageToService(String queue, String msg) {
		log.info("发送消息到集群，队列：{}，消息：{}", queue, msg);
		try {
			// instance.vertx.eventBus().publish(queue, msg);
			instance.vertx.eventBus().send(queue, msg, rf -> {
				if (rf.succeeded()) {

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sendMessageToGateWay(GameUser user, BaseMsg msg) {
		String serviceQueueName = ServiceStatusHelper.findServiceStatus(user.getGatewayServiceId())
				.getServiceQueueName();
		sendMessageToService(serviceQueueName, msg.toString());
	}

	public static void sendMessageToGame(GameUser user, BaseMsg msg) {
		String serviceQueueName = ServiceStatusHelper.findServiceStatus(user.getGameServiceId()).getServiceQueueName();
		sendMessageToService(serviceQueueName, msg.toString());
	}

	public static void broadcastMessageToService(int serviceType, BaseMsg msg) {
		List<ServiceStatus> findServiceStatus = ServiceStatusHelper.findServiceStatus(serviceType);
		for (ServiceStatus serviceStatus : findServiceStatus) {
			VertxMessageHelper.sendMessageToService(serviceStatus.getServiceQueueName(), msg.toString());
		}
	}

	public static String parseMsgToString(BaseMsg msg) {
		return null;

	}
}
