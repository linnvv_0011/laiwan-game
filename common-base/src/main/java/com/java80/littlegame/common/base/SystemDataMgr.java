package com.java80.littlegame.common.base;

import java.util.List;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java80.littlegame.common.db.dao.dbImpl.SGameConfigDaoDBImpl;
import com.java80.littlegame.common.db.dao.dbImpl.SGameInfoDaoDBImpl;
import com.java80.littlegame.common.db.entity.SGameConfig;
import com.java80.littlegame.common.db.entity.SGameInfo;

public class SystemDataMgr {
	final transient static Logger log = LoggerFactory.getLogger(SystemDataMgr.class);
	private static TreeMap<Integer, SGameInfo> gameInfos = new TreeMap<>();
	private static TreeMap<Integer, SGameConfig> gameConfigs = new TreeMap<>();

	public static void loadSystemData() {
		List<SGameInfo> sgameInfos = null;
		try {
			sgameInfos = new SGameInfoDaoDBImpl().getAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (SGameInfo i : sgameInfos) {
			gameInfos.put(i.getId(), i);
		}

		List<SGameConfig> sgameconfigs = null;
		try {
			sgameconfigs = new SGameConfigDaoDBImpl().getAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (SGameConfig i : sgameconfigs) {
			gameConfigs.put(i.getGameId(), i);
		}
	}

	public static TreeMap<Integer, SGameInfo> getGameInfos() {
		return gameInfos;
	}

	public static TreeMap<Integer, SGameConfig> getGameConfigs() {
		return gameConfigs;
	}

}
