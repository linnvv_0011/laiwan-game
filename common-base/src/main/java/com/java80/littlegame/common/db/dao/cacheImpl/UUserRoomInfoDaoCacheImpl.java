package com.java80.littlegame.common.db.dao.cacheImpl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.java80.littlegame.common.cache.CacheService;
import com.java80.littlegame.common.db.dao.UUserRoomInfoDao;
import com.java80.littlegame.common.db.dao.dbImpl.UUserRoomInfoDaoDBImpl;
import com.java80.littlegame.common.db.entity.UUserRoomInfo;

public class UUserRoomInfoDaoCacheImpl implements UUserRoomInfoDao {
	private UUserRoomInfoDaoDBImpl rdb;

	public UUserRoomInfoDaoCacheImpl(UUserRoomInfoDaoDBImpl rdb) {
		super();
		this.rdb = rdb;
	}

	@Override
	public List<UUserRoomInfo> getAllByUserId(long userId) {
		Set<UUserRoomInfo> set = CacheService.fuzzyKeys(
				CacheService.CACHEOBJPROFIX + UUserRoomInfo.class.getSimpleName() + "-*-" + userId + "-*",
				UUserRoomInfo.class);
		if (set != null && !set.isEmpty()) {
			return new ArrayList<>(set);
		} else {
			try {
				return rdb.getAllByUserId(userId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;

	}

	@Override
	public void insert(UUserRoomInfo t) {
		try {
			rdb.insert(t);
			t.save();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void delete(long id) {
		this.deleteByRoomId(Long.valueOf(id).intValue());
	}

	@Override
	public void update(UUserRoomInfo t) {
		// TODO Auto-generated method stub

	}

	@Override
	public UUserRoomInfo get(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delet(long userId, int roomId) {
		try {
			rdb.delet(userId, roomId);
			CacheService.fuzzyKeysDel(CacheService.CACHEOBJPROFIX + UUserRoomInfo.class.getSimpleName() + "-" + roomId
					+ "-" + userId + "-*");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void deleteByRoomId(int roomId) {
		try {
			rdb.deleteByRoomId(roomId);
			CacheService.fuzzyKeysDel(
					CacheService.CACHEOBJPROFIX + UUserRoomInfo.class.getSimpleName() + "-" + roomId + "-*-*");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<UUserRoomInfo> findByRoomId(int roomId) {
		Set<UUserRoomInfo> set = CacheService.fuzzyKeys(
				CacheService.CACHEOBJPROFIX + UUserRoomInfo.class.getSimpleName() + "-" + roomId + "-*-*",
				UUserRoomInfo.class);
		if (set != null && !set.isEmpty()) {
			return new ArrayList<>(set);
		} else {
			try {
				return rdb.findByRoomId(roomId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

}
