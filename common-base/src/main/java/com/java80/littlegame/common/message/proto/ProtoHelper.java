package com.java80.littlegame.common.message.proto;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.java80.littlegame.common.message.proto.cluster.DeskEndMessage;
import com.java80.littlegame.common.message.proto.cluster.GameStartMessage;
import com.java80.littlegame.common.message.proto.game.DesktopEndMessage;
import com.java80.littlegame.common.message.proto.game.caiquan.CaiquanActionMessage;
import com.java80.littlegame.common.message.proto.game.caiquan.CaiquanActionResultMessage;
import com.java80.littlegame.common.message.proto.game.caiquan.CaiquanSettleMessage;
import com.java80.littlegame.common.message.proto.game.caiquan.StartCaiquanGameMessage;
import com.java80.littlegame.common.message.proto.hall.PullAndPushGameSystemMessage;
import com.java80.littlegame.common.message.proto.hall.UserLoginMessage;
import com.java80.littlegame.common.message.proto.hall.UserRegisterMessage;
import com.java80.littlegame.common.message.proto.room.CreateRoomMessage;
import com.java80.littlegame.common.message.proto.room.JoinRoomMessage;
import com.java80.littlegame.common.message.proto.room.UserJoinRoomMessage;
import com.java80.littlegame.common.message.proto.timer.DesktopExpridedMessage;
import com.java80.littlegame.common.utils.GsonUtil;

public class ProtoHelper {

	public static BaseMsg parseJSON(String json) {
		JsonObject jobj = GsonUtil.parseJson(json);
		int code = jobj.get("code").getAsInt();
		// BaseMsg msg = JSONObject.parseObject(json, BaseMsg.class);
		return parseJSON(json, code);
	}

	public static BaseMsg parseJSON(String json, int code) {
		switch (code) {
		case ProtoList.MSG_CODE_LOGIN:
			return GsonUtil.parseJson(json, UserLoginMessage.class);
		case ProtoList.MSG_CODE_REGISTER:
			return GsonUtil.parseJson(json, UserRegisterMessage.class);
		case ProtoList.MSG_CODE_PULLANDPUSHGAMESYSTEM:
			return GsonUtil.parseJson(json, PullAndPushGameSystemMessage.class);
		case ProtoList.MSG_CODE_CREATE_ROOM:
			return GsonUtil.parseJson(json, CreateRoomMessage.class);
		case ProtoList.MSG_CODE_JION_ROOM:
			return GsonUtil.parseJson(json, JoinRoomMessage.class);
		case ProtoList.MSG_CODE_GAME_START:
			return GsonUtil.parseJson(json, GameStartMessage.class);
		case ProtoList.MSG_CODE_CAIQUANGAMESTART:
			return GsonUtil.parseJson(json, StartCaiquanGameMessage.class);
		case ProtoList.MSG_CODE_CAIQUANACTION:
			return GsonUtil.parseJson(json, CaiquanActionMessage.class);
		case ProtoList.MSG_CODE_CAIQUANACTION_RESULT:
			return GsonUtil.parseJson(json, CaiquanActionResultMessage.class);
		case ProtoList.MSG_CODE_CAIQUANASETTLE:
			return GsonUtil.parseJson(json, CaiquanSettleMessage.class);
		case ProtoList.MSG_CODE_DESKEND:
			return GsonUtil.parseJson(json, DeskEndMessage.class);
		case ProtoList.MSG_CODE_USERJOINROOM:
			return GsonUtil.parseJson(json, UserJoinRoomMessage.class);
		case ProtoList.MSG_CODE_DESKTOPEND:
			return GsonUtil.parseJson(json, DesktopEndMessage.class);
		case ProtoList.MSG_CODE_TIMER_DESKTOPEXPRIDED:
			return GsonUtil.parseJson(json, DesktopExpridedMessage.class);
		default:
			System.out.println("unkown parse msg");
			break;
		}
		return null;
	}

	public static String parseObject(Object obj) {
		return GsonUtil.parseObject(obj);
	}

	public static void main(String[] args) {
		BaseMsg fromJson = new Gson().fromJson("{\"code\":10001,\"type\":1}", UserLoginMessage.class);
		System.out.println(fromJson);// JSONObject.parseObject("{\"code\":10001,\"type\":1}",
										// BaseMsg.class));
	}

	public BaseMsg a() {
		return new UserLoginMessage();
	}
}
